import * as Parse from 'parse';
import TrashStore from './store';
export default class TrashRole extends Parse.Role {
    constructor(name: string, acl: Parse.ACL);
    get(key: string): any;
    readonly isDeleted: boolean;
    trash: TrashStore;
    removeToTrash(): Promise<this>;
    removeFromTrash(): Promise<this>;
    restoreFromTrash(): Promise<this>;
}
