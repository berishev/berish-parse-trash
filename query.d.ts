import * as Parse from 'parse';
import TrashObject from './object';
export default class TrashQuery<T extends TrashObject = TrashObject> extends Parse.Query<T> {
    constructor(objectClass: string);
    constructor(objectClass: new (...args: any[]) => T);
    private _includeTrash;
    get(objectId: string, options?: Parse.Query.GetOptions): Parse.Promise<T>;
    getInTrash(objectId: string, options?: Parse.Query.GetOptions): Parse.Promise<T>;
    getNotTrash(objectId: string, options?: Parse.Query.GetOptions): Parse.Promise<T>;
    find(options?: Parse.Query.FindOptions): Parse.Promise<T[]>;
    findInTrash(options?: Parse.Query.FindOptions): Parse.Promise<T[]>;
    findNotTrash(options?: Parse.Query.FindOptions): Parse.Promise<T[]>;
}
