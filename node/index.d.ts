import TrashObject from './object';
import TrashUser from './user';
import TrashRole from './role';
import TrashStore from './store';
import TrashQuery from './query';
export { TrashObject, TrashUser, TrashRole, TrashStore, TrashQuery };
