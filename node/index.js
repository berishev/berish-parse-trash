"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const object_1 = require("./object");
exports.TrashObject = object_1.default;
const user_1 = require("./user");
exports.TrashUser = user_1.default;
const role_1 = require("./role");
exports.TrashRole = role_1.default;
const store_1 = require("./store");
exports.TrashStore = store_1.default;
const query_1 = require("./query");
exports.TrashQuery = query_1.default;
//# sourceMappingURL=index.js.map