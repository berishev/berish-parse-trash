"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const store_1 = require("./store");
class TrashObject extends Parse.Object {
    constructor(className, options) {
        super(className, options);
    }
    get isDeleted() {
        return !!this.trash;
    }
    get trash() {
        return this.get('trash');
    }
    set trash(value) {
        this.set('trash', value);
    }
    removeToTrash() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.trash) {
                let trash = new store_1.default();
                trash.idRef = this.id;
                trash.classNameRef = this.className;
                trash.type = store_1.TrashTypeEnum.now;
                trash = yield trash.save();
                this.trash = trash;
                return this.save();
            }
            return this;
        });
    }
    removeFromTrash() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.trash) {
                let trash = this.trash;
                trash.idRef = this.id;
                trash.classNameRef = this.className;
                trash.type = store_1.TrashTypeEnum.always;
                trash = yield trash.save();
                this.trash = trash;
                return this;
            }
            return this.removeToTrash();
        });
    }
    restoreFromTrash() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.trash) {
                yield this.trash.destroy();
                this.trash = null;
            }
            return this;
        });
    }
}
exports.default = TrashObject;
//# sourceMappingURL=object.js.map