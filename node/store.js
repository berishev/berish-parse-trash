"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TrashTypeEnum;
(function (TrashTypeEnum) {
    TrashTypeEnum[TrashTypeEnum["now"] = 0] = "now";
    TrashTypeEnum[TrashTypeEnum["always"] = 1] = "always";
})(TrashTypeEnum = exports.TrashTypeEnum || (exports.TrashTypeEnum = {}));
class TrashStore extends Parse.Object {
    constructor() {
        super('TrashStore');
    }
    get idRef() {
        return this.get('idRef');
    }
    set idRef(value) {
        this.set('idRef', value);
    }
    get classNameRef() {
        return this.get('classNameRef');
    }
    set classNameRef(value) {
        this.set('classNameRef', value);
    }
    get type() {
        return TrashTypeEnum[this.get('type')];
    }
    set type(value) {
        this.set('type', TrashTypeEnum[value]);
    }
}
exports.default = TrashStore;
Parse.Object.registerSubclass('TrashStore', TrashStore);
//# sourceMappingURL=store.js.map