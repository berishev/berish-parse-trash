/// <reference types="parse" />
import TrashStore, { TrashTypeEnum } from './store';

export default class TrashObject extends Parse.Object {
  constructor(className?: string, options?: any);
  constructor(attributes?: string[], options?: any);
  constructor(className?: any, options?: any) {
    super(className, options);
  }

  get isDeleted() {
    return !!this.trash;
  }

  get trash() {
    return this.get('trash');
  }

  set trash(value: TrashStore) {
    this.set('trash', value);
  }

  async removeToTrash() {
    if (!this.trash) {
      let trash = new TrashStore();
      trash.idRef = this.id;
      trash.classNameRef = this.className;
      trash.type = TrashTypeEnum.now;
      trash = await trash.save();
      this.trash = trash;
      return this.save();
    }
    return this;
  }

  async removeFromTrash() {
    if (this.trash) {
      let trash = this.trash;
      trash.idRef = this.id;
      trash.classNameRef = this.className;
      trash.type = TrashTypeEnum.always;
      trash = await trash.save();
      this.trash = trash;
      return this;
    }
    return this.removeToTrash();
  }

  async restoreFromTrash() {
    if (this.trash) {
      await this.trash.destroy();
      this.trash = null;
    }
    return this;
  }
}
